const express = require('express');
const { body, validationResult } = require('express-validator');
const passport = require('../middleware/passportMiddleware');
const upload = require('../middleware/multerMiddleware');
const UserControler = require('../controllers/usersController');

const restrict = passport.authenticate('jwt', {
  session: false,
});
const users = new UserControler();
const app = express.Router();

// Get user's profile.
app.get('/', restrict, async (req, res) => {
  const { query } = req;
  const result = await users.get({
    id: req.user.id,
    ...query,
  });
  res.send(result);
});

// Update user's profile.
app.put(
  '/',
  restrict,
  upload.single('profilePicture'),
  [
    body('firstName')
      .notEmpty()
      .withMessage('First name cannot be empty.'),
    body('lastName')
      .notEmpty()
      .withMessage('Last name cannot be empty.'),
    body('email')
      .notEmpty()
      .withMessage('Email address cannot be empty.')
      .isEmail()
      .withMessage('Must be a valid email.')
      .custom(async (email) => {
        const existingEmail = await users.get({
          email,
        });
        if (existingEmail.length) {
          throw new Error('The email address already registered.');
        }
      }),
    body('phone')
      .notEmpty()
      .withMessage('Phone number cannot be empty.')
      .custom(async (phone) => {
        const existingPhone = await users.get({
          phone,
        });
        if (existingPhone.length) {
          throw new Error('The phone number already registered.');
        }
      }),
  ],
  async (req, res) => {
    if (req.hasOwnProperty('file_error')) {
      return res.status(400).json({
        success: false,
        message: 'One or more error have occured.',
        error: req.file_error,
      });
    }
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        success: false,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const profilePicture = req.file ? req.file.path : null;
    users.edit(req.user.id, { ...req.body, profilePicture });
    return res.status(200).json({
      success: true,
      message: 'Profile updated successfully.',
    });
  },
);

module.exports = app;
