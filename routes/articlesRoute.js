const express = require('express');
const ArticlesController = require('../controllers/articlesController');

const articles = new ArticlesController();
const app = express.Router();

// List all article.
app.get('/', async (req, res) => {
  const result = await articles.getAll();
  res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Get article detail.
app.get('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await articles.get({ id });
  res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

module.exports = app;
