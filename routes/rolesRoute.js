const express = require('express');
const { body, param, validationResult } = require('express-validator');
const passport = require('../middleware/passportMiddleware');
const verifyRole = require('../middleware/roleMiddleware');
const RolesController = require('../controllers/rolesController');

const restrict = passport.authenticate('jwt', {
  session: false,
});
const roles = new RolesController();
const app = express.Router();

// List all roles.
app.get('/', async (req, res) => {
  const result = await roles.get();
  res.status(200).json({
    status: 200,
    message: 'Success.',
    data: result,
  });
});

// Create new role (Admin).
app.post(
  '/',
  restrict,
  verifyRole(['Admin']),
  [
    body('name')
      .notEmpty()
      .withMessage('The role name cannot be empty.')
      .bail()
      .isString()
      .withMessage('The role name must be string.')
      .bail()
      .custom(async (name) => {
        const existingName = await roles.get({
          name,
        });
        if (existingName.length) {
          throw new Error('The role name already exists.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const result = await roles.add(req.body);
    return res.status(201).json({
      status: 201,
      message: 'Role created successfully.',
      data: result,
    });
  },
);

// Update role name (Admin).
app.put(
  '/:id',
  restrict,
  verifyRole(['Admin']),
  [
    param('id')
      .custom(async (id) => {
        const existingID = await roles.get({
          id,
        });
        if (!existingID.length) {
          throw new Error('Role not found.');
        }
      })
      .bail(),
    body('name')
      .notEmpty()
      .withMessage('The role name cannot be empty.')
      .bail()
      .isString()
      .withMessage('The role name must be string.')
      .bail()
      .custom(async (name) => {
        const existingName = await roles.get({
          name,
        });
        if (existingName.length) {
          throw new Error('The role name already exists.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    await roles.edit(req.params.id, req.body);
    const { id } = req.params;
    const result = await roles.get({
      id,
    });
    return res.status(200).json({
      status: 200,
      message: 'Role name updated successfully.',
      data: result,
    });
  },
);

// Delete a role (Admin).
app.delete(
  '/:id',
  restrict,
  verifyRole(['Admin']),
  [
    param('id')
      .custom(async (id) => {
        const existingID = await roles.get({
          id,
        });
        if (!existingID.length) {
          throw new Error('Role not found.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    await roles.remove(req.params.id);
    return res.status(200).json({
      status: 200,
      message: 'Role deleted successfully.',
    });
  },
);

module.exports = app;
