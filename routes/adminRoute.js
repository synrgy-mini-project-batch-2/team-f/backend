const express = require('express');
const { body, param, validationResult } = require('express-validator');
const passport = require('../middleware/passportMiddleware');
const upload = require('../middleware/multerMiddleware');
const verifyRole = require('../middleware/roleMiddleware');
const EventPaymentsController = require('../controllers/eventPaymentsController');
const ArticlesController = require('../controllers/articlesController');

const restrict = passport.authenticate('jwt', {
  session: false,
});
const eventPayments = new EventPaymentsController();
const articles = new ArticlesController();
const app = express.Router();

// EVENT PAYMENTS

// List all organizer payments.
app.get('/organizer-payments', restrict, verifyRole(['Admin']), async (req, res) => {
  const result = await eventPayments.getOrganizerPayments();
  return res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Get event payment detail.
app.get('/organizer-payments/:id', restrict, verifyRole(['Admin']), async (req, res) => {
  const { id } = req.params;
  const result = await eventPayments.get({ id });
  return res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Update event payment detail.
app.put('/organizer-payments/:id', restrict, verifyRole(['Admin']), async (req, res) => {
  const { id } = req.params;
  await eventPayments.edit(id, req.body);
  return res.status(200).json({
    success: true,
    message: 'Success.',
  });
});

// ARTICLES

// Create new article.
app.post(
  '/articles',
  restrict,
  upload.single('articleImage'),
  verifyRole(['Admin']),
  [
    body('title')
      .notEmpty()
      .withMessage('The article name cannot be empty.'),
    body('date')
      .notEmpty()
      .withMessage('Date of article cannot be empty.'),
    body('time')
      .notEmpty()
      .withMessage('Time of article cannot be empty.'),
    body('content')
      .notEmpty()
      .withMessage('Content of article cannot be empty.'),
  ],
  async (req, res) => {
    if (req.hasOwnProperty('file_error')) {
      return res.status(400).json({
        success: false,
        message: 'One or more error have occured.',
        error: req.file_error,
      });
    }
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        success: false,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const articleImage = req.file ? req.file.path : null;
    const result = await articles.add({ ...req.body, articleImage });
    return res.status(200).json({
      success: true,
      message: 'Article added successfully.',
      data: result,
    });
  },
);

// Update article'
app.put(
  '/articles/:id',
  restrict,
  upload.single('articleImage'),
  verifyRole(['Admin']),
  [
    body('title')
      .notEmpty()
      .withMessage('The article name cannot be empty.'),
    body('date')
      .notEmpty()
      .withMessage('Date of article cannot be empty.'),
    body('time')
      .notEmpty()
      .withMessage('Time of article cannot be empty.'),
    body('content')
      .notEmpty()
      .withMessage('Content of article cannot be empty.'),
  ],
  async (req, res) => {
    if (req.hasOwnProperty('file_error')) {
      return res.status(400).json({
        success: false,
        message: 'One or more error have occured.',
        error: req.file_error,
      });
    }
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        success: false,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const { id } = req.params;
    const articleImage = req.file ? req.file.path : null;
    await articles.edit(id, { ...req.body, articleImage });
    return res.status(200).json({
      success: true,
      message: 'Article edited successfully.',
    });
  },
);

// Delete a role.
app.delete(
  '/articles/:id',
  restrict,
  verifyRole(['Admin']),
  [
    param('id')
      .custom(async (id) => {
        const existingID = await articles.get({
          id,
        });
        if (!existingID.length) {
          throw new Error('Article not found.');
        }
      }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        res.status(400).json({
          success: false,
          message: error.msg,
        });
      });
    } else {
      await articles.remove(req.params.id);
      res.status(200).json({
        success: true,
        message: 'Article deleted successfully.',
      });
    }
  },
);

module.exports = app;
