const express = require('express');
const { body, validationResult } = require('express-validator');
const passport = require('../middleware/passportMiddleware');
const upload = require('../middleware/multerMiddleware');
const EventsController = require('../controllers/eventsController');
const EventPaymentsController = require('../controllers/eventPaymentsController');

const restrict = passport.authenticate('jwt', {
  session: false,
});
const events = new EventsController();
const eventPayments = new EventPaymentsController();
const app = express.Router();

// List all accepted events.
app.get('/', async (req, res) => {
  const result = await events.getAccepted();
  res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Get all past events
app.get('/past-events', async (req, res) => {
  const currentDate = new Date();
  const result = await events.getPast(currentDate);
  return res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// List all incoming (in one month) events (Admin).
app.get('/incoming-events', async (req, res) => {
  const currentDate = new Date();
  const incomingDate = new Date().setMonth(currentDate.getMonth() + 3);
  const result = await events.getIncoming(currentDate, incomingDate);
  return res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Get event detail
app.get('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await events.get({ id });
  res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Create an event
app.post(
  '/',
  restrict,
  upload.single('banner'),
  [
    body('name')
      .notEmpty()
      .withMessage('Event name cannot be empty.'),
    body('experience')
      .notEmpty()
      .withMessage('Experience cannot be empty.'),
    body('date_start')
      .notEmpty()
      .withMessage('Event start date cannot be empty.'),
    body('date_end')
      .notEmpty()
      .withMessage('Event end date cannot be empty.'),
    body('time_start')
      .notEmpty()
      .withMessage('Event start time cannot be empty.'),
    body('time_end')
      .notEmpty()
      .withMessage('Event end time cannot be empty.'),
    body('is_online')
      .notEmpty()
      .withMessage('Online status cannot be empty.'),
    body('is_paid')
      .notEmpty()
      .withMessage('Paid status cannot be empty.'),
    body('ticket_price')
      .notEmpty()
      .withMessage('Ticket price cannot be empty.'),
    body('quota')
      .notEmpty()
      .withMessage('Quota cannot be empty.'),
    body('description')
      .notEmpty()
      .withMessage('Event description cannot be empty.'),
    body('refund_policy')
      .notEmpty()
      .withMessage('Refund policy cannot be empty.'),
  ],
  async (req, res) => {
    if (req.hasOwnProperty('file_error')) {
      return res.status(400).json({
        success: false,
        message: 'One or more error have occured.',
        error: req.file_error,
      });
    }
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        success: false,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const banner = req.file ? req.file.path : null;
    const userId = req.user.id;
    const eventResult = await events.add({ ...req.body, banner, userId });
    const { id } = eventResult;
    const eventId = id;
    const status = 'Pending';
    const total_payment = 200000;
    await eventPayments.add({ status, total_payment, eventId });
    return res.status(201).json({
      success: true,
      message: 'Event created successfully',
      data: eventResult,
    });
  },
);

module.exports = app;
