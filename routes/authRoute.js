const express = require('express');
const { body, validationResult } = require('express-validator');
const UsersController = require('../controllers/usersController');
const RolesController = require('../controllers/rolesController');

const users = new UsersController();
const roles = new RolesController();
const app = express.Router();

app.post(
  '/register',
  [
    body('email')
      .notEmpty()
      .withMessage('Email cannot be empty.')
      .isEmail()
      .withMessage('Must be a valid email.')
      .custom(async (email) => {
        const existingEmail = await users.get({
          email,
        });
        if (existingEmail.length) {
          throw new Error('The email address already registered.');
        }
      }),
    body('password')
      .notEmpty()
      .withMessage('Password cannot be empty.')
      .isLength({ min: 8 })
      .withMessage('Password must be 8 characters.'),
    body('confirm_password')
      .notEmpty()
      .withMessage('Confirm password cannot be empty.')
      .custom(async (confirmPassword, { req }) => {
        const { password } = req.body;

        if (password !== confirmPassword) {
          throw new Error('Confirm password doesn\'t match.');
        }
      }),
    body('phone')
      .notEmpty()
      .withMessage('Phone number cannot be empty.')
      .custom(async (phone) => {
        const existingPhone = await users.get({
          phone,
        });
        if (existingPhone.length) {
          throw new Error('The phone number already registered.');
        }
      }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        messages.push(error.msg);
      });
      return res.status(400).json({
        success: false,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const { email, password, phone } = req.body;
    const firstName = '';
    const lastName = '';
    const profilePicture = 'uploads/profilePicture-default.jpg';
    const roleId = await roles.getUserRoleId();
    const result = await users.register(
      firstName,
      lastName,
      email,
      password,
      phone,
      profilePicture,
      roleId,
    );
    return res.status(201).json({
      success: true,
      message: 'Sign up was successful.',
      data: result,
    });
  },
);

app.post('/login', async (req, res) => {
  const { email, password } = req.body;
  const result = await users.login(email, password);
  return res.status(result[0]).json({
    success: result[1],
    message: result[2],
    data: result[3],
  });
});

module.exports = app;
