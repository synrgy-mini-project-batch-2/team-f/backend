const express = require('express');
const { body, param, validationResult } = require('express-validator');
const passport = require('../middleware/passportMiddleware');
const verifyRole = require('../middleware/roleMiddleware');
const CategoriesController = require('../controllers/categoriesController');

const restrict = passport.authenticate('jwt', {
  session: false,
});
const categories = new CategoriesController();
const app = express.Router();

// List all categories.
app.get('/', async (req, res) => {
  const result = await categories.get();
  res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

// Create new category (Admin).
app.post(
  '/',
  restrict,
  verifyRole(['Admin']),
  [
    body('name')
      .notEmpty()
      .withMessage('The category name cannot be empty.')
      .bail()
      .isString()
      .withMessage('The category name must be string.')
      .bail()
      .custom(async (name) => {
        const existingName = await categories.get({
          name,
        });
        if (existingName.length) {
          throw new Error('The category name already exists.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    const result = await categories.add(req.body);
    return res.status(201).json({
      status: 201,
      message: 'Category created successfully.',
      data: result,
    });
  },
);

// Update category name (Admin).
app.put(
  '/:id',
  restrict,
  verifyRole(['Admin']),
  [
    param('id')
      .custom(async (id) => {
        const existingID = await categories.get({
          id,
        });
        if (!existingID.length) {
          throw new Error('Category not found.');
        }
      })
      .bail(),
    body('name')
      .notEmpty()
      .withMessage('The category name cannot be empty.')
      .bail()
      .isString()
      .withMessage('The category name must be string.')
      .bail()
      .custom(async (name) => {
        const existingName = await categories.get({
          name,
        });
        if (existingName.length) {
          throw new Error('The category name already exists.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    await categories.edit(req.params.id, req.body);
    const { id } = req.params;
    const result = await categories.get({
      id,
    });
    return res.status(200).json({
      status: 200,
      message: 'Category name updated successfully.',
      data: result,
    });
  },
);

// Delete a category (Admin).
app.delete(
  '/:id',
  restrict,
  verifyRole(['Admin']),
  [
    param('id')
      .custom(async (id) => {
        const existingID = await categories.get({
          id,
        });
        if (!existingID.length) {
          throw new Error('Category not found.');
        }
      })
      .bail(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const messages = [];
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => messages.push(error.msg));
      return res.status(400).json({
        status: 400,
        message: 'One or more error have occurred.',
        error: messages,
      });
    }
    await categories.remove(req.params.id);
    return res.status(200).json({
      status: 200,
      message: 'Category deleted successfully.',
    });
  },
);

// List all events based on category.
app.get('/events/:id', async (req, res) => {
  const { id } = req.params;
  const result = await categories.getEventsByCategory(id);
  return res.status(200).json({
    success: true,
    message: 'Success.',
    data: result,
  });
});

module.exports = app;
