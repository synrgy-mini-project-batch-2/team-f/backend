const express = require('express');
const EventsController = require('../controllers/eventsController');

const events = new EventsController();
const app = express.Router();

// Search events by event name.
app.get('/', async (req, res) => {
  const { name } = req.query;
  const result = await events.getSearch(name);
  res.status(200).json({
    success: true,
    message: 'Success',
    data: result,
  });
});

module.exports = app;
