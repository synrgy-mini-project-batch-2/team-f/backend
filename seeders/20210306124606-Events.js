const { nanoid } = require('nanoid');
const faker = require('faker');

const { Roles, Users } = require('../models');

const idOrganizerList = [];

module.exports = {
  up: async (queryInterface) => {
    // Get organizer role id.
    const idOrganizer = await Roles.findOne({
      where: {
        name: 'Organizer',
      },
    }).then((role) => role.id);

    // Push all organizer id into array.
    await Users.findAll({
      where: {
        roleId: idOrganizer,
      },
    }).then((user) => user.forEach((object) => idOrganizerList.push(object.id)));

    await queryInterface.bulkInsert('Events', [
      {
        id: nanoid(),
        name: 'Technology Conference 2021',
        experience: 'Newbie',
        date_start: '2021-07-11',
        date_end: '2021-07-13',
        time_start: '10:00',
        time_end: '15:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 100,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:29:50.093Z.jpg',
        userId: idOrganizerList[0],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        name: 'Music Festival 2021',
        experience: 'Beginner',
        date_start: '2021-03-10',
        date_end: '2021-03-17',
        time_start: '10:00',
        time_end: '18:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 500,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:30:50.093Z.jpg',
        userId: idOrganizerList[1],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        name: 'Fashion Week 2021',
        experience: 'Intermediate',
        date_start: '2021-04-01',
        date_end: '2021-04-07',
        time_start: '10:00',
        time_end: '18:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 150,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:34:50.093Z.jpg',
        userId: idOrganizerList[2],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        name: 'Business Conference 2021',
        experience: 'Expert',
        date_start: '2021-03-12',
        date_end: '2021-03-15',
        time_start: '10:00',
        time_end: '15:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 200,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:40:50.093Z.jpg',
        userId: idOrganizerList[3],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        name: 'What Your Psychology Say About Your Personal Branding',
        experience: 'Expert',
        date_start: '2020-11-26',
        date_end: '2020-11-26',
        time_start: '19:00',
        time_end: '22:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 50,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:43:50.093Z.jpg',
        userId: idOrganizerList[3],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        name: 'Charity Carnival 2021',
        experience: 'Newbie',
        date_start: '2021-03-03',
        date_end: '2021-03-03',
        time_start: '09:00',
        time_end: '16:00',
        is_online: true,
        is_paid: false,
        ticket_price: 0,
        quota: 150,
        description: faker.lorem.paragraphs(),
        refund_policy: faker.lorem.paragraph(),
        banner: 'uploads/banner-2021-03-11T16:45:50.093Z.jpg',
        userId: idOrganizerList[4],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('Events', null, {});
  },
};
