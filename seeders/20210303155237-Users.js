const { nanoid } = require('nanoid');
const bcrypt = require('bcrypt');
const { Roles } = require('../models');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('Users', [
      {
        id: nanoid(),
        firstName: 'Brad',
        lastName: 'Pitt',
        email: 'bradpitt@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082111111111',
        companyOrganization: 'PT Satu',
        address: 'Jl. Maju Mundur, Yogyakarta',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:24:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Admin',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Johnny',
        lastName: 'Depp',
        email: 'johnnydepp@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082122222222',
        companyOrganization: 'PT Dua',
        address: 'Jl. Nasi Rawon, Yogyakarta',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:26:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Organizer',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Tom',
        lastName: 'Cruise',
        email: 'tomcruise@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082133333333',
        companyOrganization: 'PT Tiga',
        address: 'Jl. Kaki, Yogyakarta',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:28:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Organizer',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Margot',
        lastName: 'Robbie',
        email: 'margotrobbie@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082144444444',
        companyOrganization: 'PT Empat',
        address: 'Jl. Ayam Bakar, Surabaya',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:30:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Organizer',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Emma',
        lastName: 'Stone',
        email: 'emmastone@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082155555555',
        companyOrganization: 'PT Lima',
        address: 'Jl. Lalalayeyeye, Malang',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:33:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Organizer',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Jennifer',
        lastName: 'Aniston',
        email: 'jenniferaniston@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082166666666',
        companyOrganization: 'PT Enam',
        address: 'Jl. Sego Ndog, Yogyakarta',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:35:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'Organizer',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        firstName: 'Natalie',
        lastName: 'Portman',
        email: 'natalieportman@gmail.com',
        password: await bcrypt.hash('password', 5),
        phone: '082177777777',
        companyOrganization: 'PT Tujuh',
        address: 'Jl. Depan Belakang, Yogyakarta',
        profilePicture: 'uploads/profilePicture-2021-03-12T09:35:43.536Z.jpg',
        roleId: await Roles.findOne({
          where: {
            name: 'User',
          },
        }).then((role) => role.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('Users', null, {});
  },
};
