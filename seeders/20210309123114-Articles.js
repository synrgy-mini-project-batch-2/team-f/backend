const { nanoid } = require('nanoid');
const faker = require('faker');

const { Categories, Events } = require('../models');

const idEventList = [];

module.exports = {
  up: async (queryInterface) => {
    // Push all articles id into array
    await Events.findAll().then((event) => event.forEach((object) => idEventList.push(object.id)));

    await queryInterface.bulkInsert('Articles', [
      {
        id: nanoid(),
        title: 'Personal Branding and Psychology',
        date: '2020-11-27',
        time: '12:00',
        content: faker.lorem.paragraphs(),
        articleImage: 'uploads/articleImage-2021-03-12T22:15:14.181Z.jpg',
        eventId: idEventList[4],
        categoryId: await Categories.findOne({
          where: {
            name: 'Psychology',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        title: 'The First Charity Carnival In Town!',
        date: '2021-03-04',
        time: '12:00',
        content: faker.lorem.paragraphs(),
        articleImage: 'uploads/articleImage-2021-03-12T22:17:14.181Z.jpg',
        eventId: idEventList[5],
        categoryId: await Categories.findOne({
          where: {
            name: 'Charity & Causes',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('Articles', null, {});
  },
};
