const { Categories, Events } = require('../models');

const idEventList = [];

module.exports = {
  up: async (queryInterface) => {
    // Push all events id into array.
    await Events.findAll().then((event) => event.forEach((object) => idEventList.push(object.id)));

    await queryInterface.bulkInsert('EventsCategories', [
      {
        eventId: idEventList[0],
        categoryId: await Categories.findOne({
          where: {
            name: 'Hobbies',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        eventId: idEventList[1],
        categoryId: await Categories.findOne({
          where: {
            name: 'Entertainment',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        eventId: idEventList[2],
        categoryId: await Categories.findOne({
          where: {
            name: 'Fashion',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        eventId: idEventList[3],
        categoryId: await Categories.findOne({
          where: {
            name: 'Finance & Business',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        eventId: idEventList[4],
        categoryId: await Categories.findOne({
          where: {
            name: 'Psychology',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        eventId: idEventList[5],
        categoryId: await Categories.findOne({
          where: {
            name: 'Charity & Causes',
          },
        }).then((category) => category.id),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('EventsCategories', null, {});
  },
};
