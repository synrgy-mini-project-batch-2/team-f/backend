const { nanoid } = require('nanoid');

const { Events } = require('../models');

const idEventList = [];

module.exports = {
  up: async (queryInterface) => {
    // Push all events id into array.
    await Events.findAll().then((event) => event.forEach((object) => idEventList.push(object.id)));

    await queryInterface.bulkInsert('EventPayments', [
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[0],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[1],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[2],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[3],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[4],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: nanoid(),
        status: 'Accepted',
        total_payment: '200000',
        eventId: idEventList[5],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('EventPayments', null, {});
  },
};
