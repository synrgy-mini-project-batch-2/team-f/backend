const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'uploads/');
  },
  filename: (req, file, callback) => {
    callback(null, `${file.fieldname}-${new Date().toISOString()}${path.extname(file.originalname)}`);
  },
});

const fileFilter = (req, file, callback) => {
  const fileTypes = /jpg|jpeg/;
  const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = fileTypes.test(file.mimetype);

  if (extName && mimeType) {
    return callback(null, true);
  }
  req.file_error = 'Wrong image format. Please use jpg or jpeg image format only.';
  return callback(null, false);
};

const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter,
});

module.exports = upload;
