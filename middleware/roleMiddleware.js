const RolesController = require('../controllers/rolesController');

const roles = new RolesController();

function verifyRole(roleNames) {
  return async (req, res, next) => {
    // List all roles id from middleware parameter.
    const roleIdList = [];

    await roles.get({ name: roleNames })
      .then((role) => role.forEach((object) => roleIdList.push(object.id)));

    const { roleId } = req.user;

    if (roleIdList.includes(roleId)) {
      next();
    } else {
      res.status(401).json({
        success: false,
        message: 'One or more error have occured.',
        error: 'Unauthorized.',
      });
    }
  };
}

module.exports = verifyRole;
