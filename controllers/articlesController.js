const { Articles } = require('../models');
const BaseController = require('./baseController');

class ArticlesController extends BaseController {
  constructor() {
    super(Articles);
  }

  // List all articles by recent order.
  async getAll() {
    const allArticles = await this.model.findAll({
      attributes: ['id', 'title', 'date', 'content', 'articleImage'],
      order: [
        ['date', 'DESC'],
      ],
    });
    const payload = allArticles;
    return payload;
  }
}

module.exports = ArticlesController;
