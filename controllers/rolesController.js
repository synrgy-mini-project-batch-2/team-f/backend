const { Roles } = require('../models');
const BaseController = require('./baseController');

class RolesController extends BaseController {
  constructor() {
    super(Roles);
  }

  // Get user role id.
  async getUserRoleId() {
    const role = await this.model.findOne({
      where: {
        name: 'User',
      },
    });
    const payload = role.id;
    return payload;
  }
}

module.exports = RolesController;
