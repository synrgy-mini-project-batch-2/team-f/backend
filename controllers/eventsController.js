const { Op } = require('sequelize');
const { Events, EventPayments } = require('../models');
const BaseController = require('./baseController');

class EventsController extends BaseController {
  constructor() {
    super(Events);
  }

  // List all accepted events.
  async getAccepted() {
    const acceptedEvents = await this.model.findAll({
      include: [{
        model: EventPayments,
        as: 'payment',
        attributes: ['id', 'status'],
        where: {
          status: 'Accepted',
        },
      }],
      attributes: ['id', 'name', 'date_start', 'date_end', 'time_start', 'time_end', 'banner'],
      order: [
        ['date_start', 'DESC'],
      ],
    });
    const payload = acceptedEvents;
    return payload;
  }

  // Search events by name.
  async getSearch(name) {
    const searchResult = await this.model.findAll({
      attributes: ['id', 'name', 'date_start', 'date_end', 'time_start', 'time_end', 'banner'],
      order: [
        ['date_start', 'DESC'],
      ],
      where: {
        name: {
          [Op.iLike]: `%${name}%`,
        },
      },
      include: [{
        model: EventPayments,
        as: 'payment',
        attributes: [],
        where: {
          status: 'Accepted',
        },
      }],
    });
    const payload = searchResult;
    return payload;
  }

  // List all past events.
  async getPast(currentDate) {
    const pastEvents = await this.model.findAll({
      order: [
        ['date_start', 'DESC'],
      ],
      attributes: ['id', 'name', 'date_start', 'date_end', 'time_start', 'time_end', 'banner'],
      where: {
        date_end: {
          [Op.lt]: currentDate,
        },
      },
      include: [{
        model: EventPayments,
        as: 'payment',
        where: {
          status: 'Accepted',
        },
        attributes: [],
      }],
    });
    const payload = pastEvents;
    return payload;
  }

  // List all incoming events.
  async getIncoming(currentDate, incomingDate) {
    const incomingEvents = await this.model.findAll({
      where: {
        date_start: {
          [Op.between]: [currentDate, incomingDate],
        },
      },
      attributes: ['id', 'name', 'date_start', 'date_end', 'time_start', 'time_end', 'banner'],
      order: [
        ['date_start', 'ASC'],
      ],
      include: [{
        model: EventPayments,
        as: 'payment',
        where: {
          status: 'Accepted',
        },
        attributes: [],
      }],
    });

    const payload = incomingEvents;
    return payload;
  }
}

module.exports = EventsController;
