const { Categories, Events, EventPayments } = require('../models');
const BaseController = require('./baseController');

class CategoriesController extends BaseController {
  constructor() {
    super(Categories);
  }

  // List all events based on category.
  async getEventsByCategory(id) {
    const eventsCategory = await this.model.findByPk(id, {
      attributes: ['id', 'name'],
      include: [{
        model: Events,
        as: 'events',
        attributes: ['id', 'name', 'date_start', 'date_end', 'time_start', 'time_end', 'banner'],
        include: [{
          model: EventPayments,
          as: 'payment',
          attributes: [],
          where: {
            status: 'Accepted',
          },
        }],
      }],
    });
    const payload = eventsCategory;
    return payload;
  }
}

module.exports = CategoriesController;
