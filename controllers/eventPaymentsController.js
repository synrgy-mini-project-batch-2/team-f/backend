const { Events, EventPayments, Users } = require('../models');
const BaseController = require('./baseController');

class EventPaymentsController extends BaseController {
  constructor() {
    super(EventPayments);
  }

  // List all organizer payments.
  async getOrganizerPayments() {
    const organizerPayments = await this.model.findAll({
      include: [{
        model: Events,
        as: 'event',
        attributes: ['id', 'name', 'userId'],
        include: [{
          model: Users,
          as: 'users',
          attributes: ['email'],
        }],
      }],
      attributes: ['id', 'status'],
    });
    const payload = organizerPayments;
    return payload;
  }
}

module.exports = EventPaymentsController;
