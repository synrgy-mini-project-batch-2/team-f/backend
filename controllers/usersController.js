const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { nanoid } = require('nanoid');
const { Users } = require('../models');
const BaseController = require('./baseController');

class UsersController extends BaseController {
  constructor() {
    super(Users);
  }

  async register(firstName, lastName, email, password, phone, photoProfile, roleId) {
    const encryptedPassword = await bcrypt.hash(password, 5);
    const id = nanoid();
    const payload = {
      id,
      firstName,
      lastName,
      email,
      password: encryptedPassword,
      phone,
      photoProfile,
      roleId,
    };
    await this.model.create({
      ...payload,
    });
    payload.token = jwt.sign({ id }, process.env.JWT_SECRET);
    return payload;
  }

  async login(email, password) {
    const user = await this.model.findOne({
      where: {
        email,
      },
    });

    if (user === null) {
      const status = 400;
      const success = false;
      const message = 'One or more error have occured.';
      const error = 'Incorrect email or password.';
      return [status, success, message, error];
    }

    if (await bcrypt.compare(password, user.password)) {
      const status = 200;
      const success = true;
      const message = 'Login was successful.';
      const payload = {
        id: user.id,
        email: user.email,
        token: jwt.sign({ id: user.id }, process.env.JWT_SECRET),
      };
      return [status, success, message, payload];
    }
    const status = 400;
    const success = false;
    const message = 'One or more error have occured.';
    const error = 'Incorrect email or password.';
    return [status, success, message, error];
  }
}

module.exports = UsersController;
