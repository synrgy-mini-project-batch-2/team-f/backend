const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Articles extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Articles.belongsTo(models.Events, {
        as: 'events',
        foreignKey: 'eventId',
      });
      Articles.belongsTo(models.Categories, {
        as: 'categories',
        foreignKey: 'categoryId',
      });
    }
  }
  Articles.init({
    title: DataTypes.STRING,
    date: DataTypes.DATEONLY,
    time: DataTypes.TIME,
    content: DataTypes.TEXT,
    articleImage: DataTypes.STRING,
    eventId: DataTypes.STRING(21),
    categoryId: DataTypes.STRING(21),
  }, {
    sequelize,
    modelName: 'Articles',
  });
  return Articles;
};
