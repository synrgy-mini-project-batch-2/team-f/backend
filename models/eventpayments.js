const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class EventPayments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      EventPayments.belongsTo(models.Events, {
        as: 'event',
        foreignKey: 'eventId',
      });
    }
  }
  EventPayments.init({
    status: DataTypes.STRING,
    total_payment: DataTypes.INTEGER,
    eventId: DataTypes.STRING(21),
  }, {
    sequelize,
    modelName: 'EventPayments',
  });
  return EventPayments;
};
