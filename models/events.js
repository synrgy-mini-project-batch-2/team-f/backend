const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Events extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Events.belongsToMany(models.Categories, {
        through: 'EventsCategories',
        as: 'categories',
        foreignKey: 'eventId',
      });
      Events.belongsTo(models.Users, {
        as: 'users',
        foreignKey: 'userId',
      });
      Events.hasOne(models.EventPayments, {
        as: 'payment',
        foreignKey: 'eventId',
      });
    }
  }
  Events.init({
    name: DataTypes.STRING,
    experience: DataTypes.STRING,
    date_start: DataTypes.DATEONLY,
    date_end: DataTypes.DATEONLY,
    time_start: DataTypes.TIME,
    time_end: DataTypes.TIME,
    is_online: DataTypes.BOOLEAN,
    is_paid: DataTypes.BOOLEAN,
    ticket_price: DataTypes.INTEGER,
    quota: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    refund_policy: DataTypes.TEXT,
    banner: DataTypes.STRING,
    userId: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Events',
  });
  return Events;
};
