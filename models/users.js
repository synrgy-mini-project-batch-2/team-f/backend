const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Users.hasOne(models.Roles, { foreignKey: 'id' });
      Users.hasMany(models.Events, {
        as: 'events',
        foreignKey: 'userId',
      });
    }
  }
  Users.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    phone: DataTypes.STRING,
    companyOrganization: DataTypes.STRING,
    address: DataTypes.STRING,
    profilePicture: DataTypes.STRING,
    roleId: DataTypes.STRING(21),
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};
