require('dotenv').config();
const express = require('express');
const cors = require('cors');
const passport = require('./middleware/passportMiddleware');

const app = express();
app.use(passport.initialize());
app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set('view engine', 'ejs');
app.use(express.static('views'));
app.use('/uploads', express.static('uploads'));

app.use('/', require('./routes/indexRoute'));
app.use('/auth', require('./routes/authRoute'));
app.use('/api/roles', require('./routes/rolesRoute'));
app.use('/api/profile', require('./routes/usersRoute'));
app.use('/api/categories', require('./routes/categoriesRoute'));
app.use('/api/events', require('./routes/eventsRoute'));
app.use('/api/articles', require('./routes/articlesRoute'));
app.use('/api/admin', require('./routes/adminRoute'));
app.use('/search', require('./routes/searchRoute'));

app.use((req, res, next) => {
  res.status(404).send('Page not found');
});

const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server is listening on http://localhost:${port}`);
});
