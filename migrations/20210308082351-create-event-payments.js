module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EventPayments', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING(21),
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      total_payment: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      eventId: {
        type: Sequelize.STRING(21),
        references: {
          model: 'Events',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('EventPayments');
  },
};
