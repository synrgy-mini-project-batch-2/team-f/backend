module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Articles', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING(21),
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      date: {
        allowNull: false,
        type: Sequelize.DATEONLY,
      },
      time: {
        type: Sequelize.TIME,
      },
      content: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      articleImage: {
        type: Sequelize.STRING,
      },
      eventId: {
        type: Sequelize.STRING,
        references: {
          model: 'Events',
          key: 'id',
        },
      },
      categoryId: {
        type: Sequelize.STRING,
        references: {
          model: 'Categories',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Articles');
  },
};
