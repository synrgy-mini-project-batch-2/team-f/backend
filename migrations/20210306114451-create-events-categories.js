module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EventsCategories', {
      eventId: {
        type: Sequelize.STRING(21),
        references: {
          model: 'Events',
          key: 'id',
        },
      },
      categoryId: {
        type: Sequelize.STRING(21),
        references: {
          model: 'Categories',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('EventsCategories');
  },
};
