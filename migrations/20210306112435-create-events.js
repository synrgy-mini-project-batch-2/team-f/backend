module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Events', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING(21),
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      experience: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      date_start: {
        allowNull: false,
        type: Sequelize.DATEONLY,
      },
      date_end: {
        allowNull: false,
        type: Sequelize.DATEONLY,
      },
      time_start: {
        allowNull: false,
        type: Sequelize.TIME,
      },
      time_end: {
        allowNull: false,
        type: Sequelize.TIME,
      },
      is_online: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      is_paid: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      ticket_price: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      quota: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      refund_policy: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      banner: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      userId: {
        type: Sequelize.STRING,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Events');
  },
};
